#! /usr/bin/env python
# -*- coding: utf-8 -*-
import smtplib, sys, time
from cgi import parse_qs, escape
from email.MIMEBase import MIMEBase
from email.MIMEMultipart import MIMEMultipart
from email.mime.text import MIMEText
import recaptcha2

def application(environ, start_response):
    start_response("200 OK", [
        ("Content-type", "text/plain"),
        ('Access-Control-Allow-Origin', "http://localhost:8000"),
        ('Access-Control-Allow-Headers', "Origin, X-Requested-With, Content-Type")
        ])
    
    try:
        request_body_size = int(environ.get('CONTENT_LENGTH', 0))
    except (ValueError):
        request_body_size = 0

    titulo = ''
    cuerpo = ''

    if request_body_size > 0:
        request_body = environ['wsgi.input'].read(request_body_size)

        d = parse_qs(request_body)

        # Obtenemos los valores del formulario
        name = d.get('name', [''])[0]
        email = d.get('email', [''])[0]
        antispam = d.get('antispam', [''])[0]
        phone = d.get('phone', [''])[0]
        service = d.get('service', [''])[0]
        message = d.get('message', [''])[0]
        more_info = d.get('more-info', [])
        recaptcha = d.get('recaptcha', [''])[0]

        # Saneamos los inputs
        name = escape(name)
        email = escape(email)
        antispam = escape(antispam)
        phone = escape(phone)
        service = escape(service)
        message = escape(message)
        more_info = ("Observaciones: deseo recibir información sobre otras ofertas.", "")[len(more_info) == 0]

        # Texto del correo electronico
        titulo = "[%(service)s] Petición de información" % { "service": service }
        cuerpo = '''
        Nombre: %(name)s <br>
        Email: %(email)s <br>
        Teléfono: %(phone)s <br><br>
        Mensaje: <br>========<br><br>
        %(message)s 
        <br><br>
        %(more_info)s
        <br><br>
        --Correo enviado desde el formulario web de Hispasec.com
        ''' % {
            "name": name, 
            "email": email, 
            "service": service, 
            "message": message.replace("\n", "<br>"), 
            "phone": phone, 
            "more_info": more_info
            }

        # RECAPTCHA
        secret = '6LcYPiEUAAAAAGeECVqIJMlBIZvQcjDlIvyWeUia'
        response = recaptcha2.verify(secret, recaptcha)

        if response['success']:
            envia(titulo, cuerpo, 'info@hispasec.com', email)
            return ['Ok']
        else:
            return ['Invalid captcha']
    else:
        return ['Error']

def envia( titulo, html, destino, me):
    msg = MIMEMultipart()
    msg['Subject'] = titulo

    import email.Utils
    msg['Date'] = email.Utils.formatdate()
    msg['From'] = me
    msg['To'] = destino

    htmlAdj=MIMEBase('text','html')
    htmlAdj.set_payload(html)
    msg.attach(htmlAdj)

    SMTP_SERVER='paperboy.hispasec.com'
    SMTP_USER='web@paperboy.hispasec.com'
    SMTP_PASS='US2Rg5f1lhQ7'
    smtp = smtplib.SMTP(SMTP_SERVER, 25)

    smtp.starttls()
    smtp.ehlo()
    smtp.login(SMTP_USER, SMTP_PASS)
    smtp.sendmail(me, [destino], msg.as_string())
    smtp.close()